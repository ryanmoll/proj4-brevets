# Project 4: Brevet time calculator with Ajax

_____________________________________

Contact:

- Name: Ryan Moll

- Email: rmoll@uoregon.edu

_____________________________________

Info:

- The working parts of the code are comprised by three files. These are acp_times.py, flask_brevets.py, and calc.html. These files produce a 
Flask-based webserver and host an interactive html page on it. The webpage takes various inputs and produce output times based on those inputs. The specifications for these calculations can be found here: https://rusa.org/pages/acp-brevet-control-times-calculator

- There is also a test folder which contains a test file. To run that test file, use the command 'nosetests' from the /brevets directory.

- All of this code can be packaged up using Docker. Should you want to run this code with docker, follow the standard 'docker build .' and 'docker run' commands like you normally would, along with any flags you want to utelize.

_____________________________________

Considerations:

- Hours and minutes is not sufficient to measure the distances being calculated. Since we are truncating the seconds value, we must either round our minutes up, down, or have it vary by the value of minutes. I chose to round up if there were more than 30 seconds and down if there were less. This is how the ACP calculator handled this case.

- In the provided chart, the boundaries of the chart are listed twice. E.g. for 0-200, the speed is 34 and for 200-400, the speed is 32. Since 200 is listed twice, what speed do you use if the distance is 200? As per the ACP specifications, you use the faster of the two speeds. 

- If the distance of a checkpoint is slightly over or under the distance of the entire brevet can we still use it? On the ACP website, the final checkpoint must be at least as far as the entire brevet distance. They also specify that the final checkpoint CAN go past the total distance of the brevet as long as it does not exceed 20% beyond the length of the entire brevet.

	- In the case that a checkpoint does exceed that 20% limit, I have the html page update with a warning message and the Open time and Close time fields clear.

- The ACP website specifies a few special cases as well that my code handles. All of these pertain to calculating the Close time. Case 1 is a start checkpoint- a checkpoint at 0km. In this case, the start time is shifted forward 0 hours and the close time is shifted forward an hour. The other case is when the control distance is greter than or equal to the brevet distance. ACP defines this case on their website with the following text: 
	"The closing time for the finish checkpoint is calculated by adding the maximum permitted time for the brevet to the opening time of the start checkpoint. Maximum permitted times (in hours and minutes, HH:MM) are 13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 40:00 for 600 KM, and 75:00 for 1000 KM."
In turn, when the control distance matches the total brevet distance my code shifts the close time based on these given hour shifts.