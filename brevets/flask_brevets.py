"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', -1, type=float)                         # Retrieve the km val passed in. Default to -1
    begin_time = request.args.get('begin_time')                         # Retrieve the begin_time val passed in
    begin_date = request.args.get('begin_date')                         # Retrieve the begin_date val passed in
    distance = request.args.get('distance')                             # Retrieve the distance val passed in
    app.logger.debug("request.args: {}".format(request.args))

    if km == -1:
        result = {"error": 2}
        return flask.jsonify(result=result)

    distance = float(distance)                                          # Convert distance form string to float so we can perform operations on it
    if km > distance * 1.2:                                             # More than 20% greater than the total brevet length is our cutoff
        result = {"error": 1}
        return flask.jsonify(result=result)                             # Abort now with error

    time = begin_date + " " + begin_time
    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)

    result = {"open": open_time, "close": close_time, "error": 0}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
