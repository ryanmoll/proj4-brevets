"""
Nose tests for acp_times.py
"""
from acp_times import open_time, close_time
import nose    # Testing framework

# NOTE: THE FLASK_BREVETS.PY FILE HANDLES CASES WHERE THE CHECKPOINT DISTANCE IS TOO BIG. WILL NOT TEST THAT CASE WITH NOSETESTS.
def test_zero():
    assert open_time(0, 400, "2018-01-01 00:00") == "2018-01-01T00:00:00-08:00"
    assert close_time(0, 400, "2018-01-01 00:00") == "2018-01-01T01:00:00-08:00"

def test_year_end():
    assert open_time(150, 400, "2017-12-31 22:00") == "2018-01-01T02:25:00-08:00"
    assert close_time(150, 400, "2017-12-31 22:00") == "2018-01-01T08:00:00-08:00"

def test_boundary():
    assert open_time(600, 600, "2018-05-20 10:00") == "2018-05-21T04:48:00-07:00"
    assert close_time(600, 600, "2018-05-20 10:00") == "2018-05-22T02:00:00-07:00"

def test_beyond_boundary():
    assert open_time(205, 200, "2017-08-21 13:00") == "2017-08-21T18:53:00-07:00"
    assert close_time(205, 200, "2017-08-21 13:00") == "2017-08-22T02:30:00-07:00"

def test_leap_year():
    assert open_time(250, 300, "2016-02-28 22:00") == "2016-02-29T05:27:00-08:00"
    assert close_time(250, 300, "2016-02-28 22:00") == "2016-02-29T14:40:00-08:00"